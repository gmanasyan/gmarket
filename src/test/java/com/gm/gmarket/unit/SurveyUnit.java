package com.gm.gmarket.unit;

import com.gm.gmarket.domain.SurveyService;
import com.gm.gmarket.domain.model.Option;
import com.gm.gmarket.domain.model.Question;
import com.gm.gmarket.domain.model.Survey;
import com.gm.gmarket.exception.ErrorCodes;
import com.gm.gmarket.infrastructure.jpa.OptionRepository;
import com.gm.gmarket.infrastructure.jpa.QuestionRepository;
import com.gm.gmarket.infrastructure.jpa.SurveyRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import java.util.List;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit tests
 */
@ExtendWith(MockitoExtension.class)
public class SurveyUnit {

    @Mock
    private SurveyRepository surveyRepository;

    @Mock
    private QuestionRepository questionRepository;

    @Mock
    private OptionRepository optionRepository;

    private SurveyService surveyService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        surveyService = new SurveyService(surveyRepository);
    }

    @Test
    public void addNewSurvey() {
        // Given
        Survey survey = Survey.builder()
                .name("Survey 01")
                .questions(List.of(
                        Question.builder().title("Q")
                                .options(List.of(Option.builder().description("Option a").build(),
                                        Option.builder().description("Option b").build()))
                                .build(),
                        Question.builder().title("Question 02")
                                .options(List.of( Option.builder().description("Option aa").build(),
                                        Option.builder().description("Option bb").build()))
                                .build())
                )
                .build();

        var id = UUID.randomUUID();
        when(surveyRepository.save(any())).thenAnswer(new Answer<Survey>() {
            @Override
            public Survey answer(InvocationOnMock invocation) throws Throwable {
                Survey survey = (Survey) invocation.getArgument(0);
                survey.setId(id);
                return survey;
            }
        });

        // When
        UUID result = surveyService.addSurvey(survey);

        // Then
        Assertions.assertEquals(id, result);
        verify(surveyRepository, times(1)).save(any());
    }

    @Test
    public void getSurvey() {
        // Given

        // When
        Exception exception = Assertions.assertThrows(RuntimeException.class, () -> {
            surveyService.getSurvey(UUID.randomUUID());
        });

        // Then
        Assertions.assertEquals(ErrorCodes.ERROR_NOT_FOUND.name(), exception.getMessage());
        verify(questionRepository, times(0)).saveAll(any());
    }

}
