package com.gm.gmarket;

import com.gm.gmarket.infrastructure.jpa.OptionRepository;
import com.gm.gmarket.infrastructure.jpa.QuestionRepository;
import com.gm.gmarket.infrastructure.jpa.SurveyRepository;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.filter.CharacterEncodingFilter;

/**
 * For full spring boot test context.
 */
@SpringBootTest
@ActiveProfiles({ "test" })
public abstract class BaseAbstractTest {

    public static final CharacterEncodingFilter CHARACTER_ENCODING_FILTER = new CharacterEncodingFilter();

    static {
        CHARACTER_ENCODING_FILTER.setEncoding("UTF-8");
        CHARACTER_ENCODING_FILTER.setForceEncoding(true);
    }

    @Autowired
    protected SurveyRepository surveyRepository;

    @Autowired
    protected QuestionRepository questionRepository;

    @Autowired
    protected OptionRepository optionRepository;


    @BeforeEach
    public void clearDb() {
        surveyRepository.deleteAll();
        questionRepository.deleteAll();
        optionRepository.deleteAll();
    }
}
