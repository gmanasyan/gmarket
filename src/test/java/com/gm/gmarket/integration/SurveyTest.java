package com.gm.gmarket.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gm.gmarket.BaseAbstractTest;
import com.gm.gmarket.presentation.dto.OptionDto;
import com.gm.gmarket.presentation.dto.QuestionDto;
import com.gm.gmarket.presentation.dto.SurveyDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class SurveyTest extends BaseAbstractTest {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;

    final String contextPath = "/survey";

    @BeforeEach
    public void setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(context)
                .addFilter(CHARACTER_ENCODING_FILTER)
                .build();
    }

    @Test
    public void addSurvey() throws Exception {
        // Given
        SurveyDto surveyDto = SurveyDto.builder()
                .name("Survey 01")
                .questions(List.of(
                        QuestionDto.builder().title("Question 01")
                                .options(List.of(new OptionDto(null, "Option a"),
                                        new OptionDto(null, "Option b")))
                                .build(),
                        QuestionDto.builder().title("Question 02")
                                .options(List.of(new OptionDto(null, "Option aa"),
                                        new OptionDto(null, "Option bb")))
                                .build())
                )
                .build();

        // When
        ResultActions result = mvc.perform(post(contextPath)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(surveyDto)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));

        // Then 1
        UUID surveyId = objectMapper.readValue(result.andReturn().getResponse().getContentAsString(),
                UUID.class);

        assertNotNull(surveyId);
    }

}
