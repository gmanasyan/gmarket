--liquibase formatted sql
--changeset gmanasyan:create-table-option

create table options
(
    id   UUID primary key,
    description varchar(250) not null,
    question_id  uuid not null
        constraint fk_options_question
            references questions
);

comment on table options is 'Answer option info';
comment on column options.id is 'internal id in the system';
comment on column options.description is 'Answer option description';
