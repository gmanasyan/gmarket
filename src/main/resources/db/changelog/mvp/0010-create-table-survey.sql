--liquibase formatted sql
--changeset gmanasyan:create-table-survey

create table surveys
(
    id   uuid primary key,
    name varchar(100) not null
);

comment on table surveys is 'Survey info';
comment on column surveys.id is 'internal id in the system';
comment on column surveys.name is 'survey name';
