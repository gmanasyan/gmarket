--liquibase formatted sql
--changeset gmanasyan:create-table-question

create table questions
(
    id   uuid primary key,
    title varchar(100) not null,
    active boolean default true,
    survey_id  uuid not null
        constraint fk_question_survey
            references surveys
);

comment on table questions is 'Question info';
comment on column questions.id is 'internal id in the system';
comment on column questions.title is 'question name';
comment on column questions.active is 'is active';
