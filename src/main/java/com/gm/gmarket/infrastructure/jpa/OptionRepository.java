package com.gm.gmarket.infrastructure.jpa;

import com.gm.gmarket.domain.model.Option;
import com.gm.gmarket.domain.model.Survey;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface OptionRepository extends JpaRepository<Option, UUID> {
}
