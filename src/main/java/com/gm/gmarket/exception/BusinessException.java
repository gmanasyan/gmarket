package com.gm.gmarket.exception;

public class BusinessException extends RuntimeException{

    public BusinessException(ErrorCodes message) {
        super(message.name());
    }

}
