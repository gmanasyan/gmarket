package com.gm.gmarket.domain;

import com.gm.gmarket.domain.model.Survey;
import com.gm.gmarket.exception.BusinessException;
import com.gm.gmarket.exception.ErrorCodes;
import com.gm.gmarket.infrastructure.jpa.SurveyRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Slf4j
@Service
@AllArgsConstructor
public class SurveyService {

    private final SurveyRepository surveyRepository;

    @Transactional
    public UUID addSurvey(Survey survey) {
        log.info("---> New survey " + survey);
        survey.getQuestions().forEach(q -> q.setSurvey(survey));
        survey.getQuestions().forEach(q -> q.getOptions().forEach(o -> o.setQuestion(q)));
        surveyRepository.save(survey);
        return survey.getId();
    }

    @Transactional(readOnly = true)
    public Survey getSurvey(UUID id) {
        return surveyRepository.findById(id).orElseThrow(
                () -> new BusinessException(ErrorCodes.ERROR_NOT_FOUND));
    }
}
