package com.gm.gmarket.domain.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.UUID;

@Getter
@Setter
@MappedSuperclass
abstract class Identity {

    @Id
    private UUID id = UUID.randomUUID();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Identity)) return false;
        return id != null && id.equals(((Identity) o).getId());
    }

    @Override
    public int hashCode() { return 31;
    }

}
