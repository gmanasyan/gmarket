package com.gm.gmarket.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "surveys")
public class Survey extends Identity {

    @NotNull
    @Length(min = 3, max = 100, message = "min 3 and 100 characters max")
    private String name;

    @OneToMany(mappedBy = "survey", cascade = {CascadeType.ALL}, orphanRemoval = true)
    private List<Question> questions = new ArrayList<>();

    public Survey(String name) {
        this.name = name;
    }

    public void addQuestion(Question question) {
        questions.add(question);
        question.setSurvey(this);
    }

    public void removeQuestion(Question question) {
        questions.remove(question);
        question.setSurvey(null);
    }

}
