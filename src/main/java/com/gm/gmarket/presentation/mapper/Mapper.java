package com.gm.gmarket.presentation.mapper;

import org.mapstruct.factory.Mappers;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Mapper {

    @Bean
    public SurveyMapper getSurveyMapper() {
        return Mappers.getMapper(SurveyMapper.class);
    }


}
