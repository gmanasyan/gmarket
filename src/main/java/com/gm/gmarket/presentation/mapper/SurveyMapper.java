package com.gm.gmarket.presentation.mapper;

import com.gm.gmarket.domain.model.Option;
import com.gm.gmarket.domain.model.Question;
import com.gm.gmarket.domain.model.Survey;
import com.gm.gmarket.presentation.dto.OptionDto;
import com.gm.gmarket.presentation.dto.QuestionDto;
import com.gm.gmarket.presentation.dto.SurveyDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public abstract class SurveyMapper {

    public abstract Survey toSurvey(SurveyDto source);

    @Mapping(target = "active", constant = "true")
    @Mapping(target = "survey", expression = "java(null)")
    public abstract Question toQuestion(QuestionDto source);

    @Mapping(target = "question", expression = "java(null)")
    public abstract Option toOption(OptionDto source);


    public abstract SurveyDto toSurveyDto(Survey source);
    public abstract QuestionDto toQuestionDto(Question source);
    public abstract OptionDto toOptionDto(Option source);

}
