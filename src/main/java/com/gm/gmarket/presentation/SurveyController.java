package com.gm.gmarket.presentation;


import com.gm.gmarket.domain.SurveyService;
import com.gm.gmarket.presentation.dto.SurveyDto;
import com.gm.gmarket.presentation.mapper.SurveyMapper;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.UUID;

@Service
@RestController
@AllArgsConstructor
@RequestMapping("survey")
public class SurveyController {

    private final SurveyService surveyService;

    private final SurveyMapper surveyMapper;

    @PostMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE )
    public UUID addSurvey(@Valid @RequestBody SurveyDto surveyRq) {
        return surveyService.addSurvey(surveyMapper.toSurvey(surveyRq));
    }

    @Transactional(readOnly = true)
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE )
    public SurveyDto getSurvey(@PathVariable("id") UUID id) {
        return surveyMapper.toSurveyDto(surveyService.getSurvey(id));
    }
}
