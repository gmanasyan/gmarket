package com.gm.gmarket.presentation.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@Data
@Valid
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class QuestionDto {

    UUID id;

    @NotNull
    @Length(min = 3, max = 100, message = "min 3 and 100 characters max")
    String title;

    @NotNull
    List<OptionDto> options;

}
