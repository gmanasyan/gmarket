package com.gm.gmarket.presentation.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@Valid
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OptionDto {

    UUID id;

    @NotNull
    @Length(min = 3, max = 250, message = "min 3 and 250 characters max")
    String description;

}
