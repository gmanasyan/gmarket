# GMarket
Survey service.

### 01 Add survey
To add survey please run the follow

```shell
curl --location --request POST "http://localhost:8080/survey" --header 'Content-Type: application/json' --data-raw '{
    "name": "First survey",
    "questions": [
        {
            "title" : "Question 01",
            "options": [
                {
                    "description": "Option a"
                },
                {
                    "description": "Option b"
                },
                {
                    "description": "Option c"
                }
            ]
        },
                {
            "title" : "Question 02",
            "options": [
                {
                    "description": "Option aa"
                },
                {
                    "description": "Option bb"
                },
                {
                    "description": "Option cc"
                }
            ]
        }

    ]

}'
```

### 02 Get survey
To get survey
```shell
curl --location --request GET "http://localhost:8080/survey/1"
```
Answer example
```json
{
    "id": "d3f626b9-7cc5-44fc-8c44-0041cd502afd",
    "name": "First survey",
    "questions": [
        {
            "id": "278b580d-9d73-4ba6-b885-48c352b086e1",
            "title": "Question 01",
            "options": [
                {
                    "id": "a4a10eed-afac-4249-8bd8-ee0f0a51e86c",
                    "description": "Option a"
                },
                {
                    "id": "a2f773bb-0fdd-4b95-b1bc-b6d3b6005a2b",
                    "description": "Option b"
                },
                {
                    "id": "181d0257-77f8-41ab-b6fa-90cc983993a2",
                    "description": "Option c"
                }
            ]
        },
        {
            "id": "6923fe66-9489-4414-ba22-21b4ddbd89ea",
            "title": "Question 02",
            "options": [
                {
                    "id": "aae69c2f-7cd0-4e34-95d7-62655e954ce2",
                    "description": "Option aa"
                },
                {
                    "id": "8ab40fc6-bb79-4fb1-a9ce-74d8de547544",
                    "description": "Option bb"
                },
                {
                    "id": "eeeff423-dd8e-4597-a0bd-9f55335b6357",
                    "description": "Option cc"
                }
            ]
        }
    ]
}
```
